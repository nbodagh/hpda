#!/usr/bin/env python3

import sys

def main():
    # Initialize variables
    current_key = None
    current_count = 0
    # Process input from stdin
    for line in sys.stdin:
        line = line.strip()
        if line:
            # Split the line into key, filename, word, and count
            key, filename, word, count = line.split(',')
            # If the key has changed, emit the current key and count
            if key != current_key:
                if current_key is not None:
                    print(f'{current_key},{current_count}')
                current_key = key
                current_count = 0
            # Add the count to the current count
            current_count += int(count)
    # Emit the last key and count
    if current_key is not None:
        print(f'{current_key},{current_count}')

if __name__ == '__main__':
    main()

