from pyspark import SparkConf, SparkContext

conf = SparkConf().setAppName("Crime Analysis")
sc = SparkContext.getOrCreate(conf=conf)

# Initialize accumulators
crime_counts = sc.accumulator(0)
burglary_counts = sc.accumulator(0)

# Load CSV file into RDD and count occurrences using accumulators
def count_crime(line):
    global crime_counts, burglary_counts
    fields = line.split(",")
    crimedescr = fields[5]
    if crimedescr:
        crime_counts += 1
        if "BURGLARY" in crimedescr:
            burglary_counts += 1
    return line

rdd = sc.textFile("SacramentocrimeJanuary2006.csv")
rdd.map(count_crime).count()

# Print results
print("Total crimes:", crime_counts.value)
print("Burglary crimes:", burglary_counts.value)

