from pyspark.sql import SparkSession
from pyspark.sql.functions import count

spark = SparkSession.builder.appName("Crime Analysis").getOrCreate()

# Load CSV file into data frame
df = spark.read.format("csv").option("header", "true").load("SacramentocrimeJanuary2006.csv")

# Count occurrences using SQL functionality
df.groupBy("crimedescr").agg(count("*").alias("frequency")).orderBy("frequency", ascending=False).show()

spark.stop()

