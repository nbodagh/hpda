from pyspark import SparkConf, SparkContext

conf = SparkConf().setAppName("Crime Analysis")
sc = SparkContext.getOrCreate(conf=conf)

# Load CSV file into RDD
rdd = sc.textFile("SacramentocrimeJanuary2006.csv")

# Extract crimedescr column and count occurrences
crimes = rdd.map(lambda line: line.split(",")[5])
crime_counts = crimes.countByValue()

# Save crimedescr and frequency information into a CSV file
with open("crime_counts.csv", "w") as file:
    file.write("crimedescr,frequency\n")
    for crime, count in crime_counts.items():
        file.write("{},{}\n".format(crime, count))

