import json

# Open the JSON file and load the word counts
with open('word_counts.json', 'r') as f:
    word_counts = json.load(f)

# Combine the word counts from all chapters into one dictionary
all_word_counts = {}
for chapter_word_counts in word_counts.values():
    for word, count in chapter_word_counts.items():
        all_word_counts[word] = all_word_counts.get(word, 0) + count

# Sort the dictionary by value and print the top 10 words
sorted_word_counts = sorted(all_word_counts.items(), key=lambda x: x[1], reverse=True)
for word, count in sorted_word_counts[:10]:
    print(f'{word}: {count}')

