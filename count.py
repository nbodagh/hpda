import matplotlib
import argparse
import string
import matplotlib.pyplot as plt

# Define command line argument for the word to search for
parser = argparse.ArgumentParser()
parser.add_argument("word", help="the word to search for in the book")
args = parser.parse_args()

# Open and read the book
with open("mobydick.txt", "r") as f:
    book = f.read()

# Split the book into chapters
chapters = book.split("CHAPTER ")

# Initialize a list to store word frequencies
freqs = []

# Loop through each chapter
for i in range(1, len(chapters)):
    # Remove punctuation and convert to lowercase
    chapter = chapters[i].translate(str.maketrans("", "", string.punctuation)).lower()
    # Split chapter into words
    words = chapter.split()
    # Count occurrences of the given word
    count = words.count(args.word.lower())
    # Calculate relative frequency
    freq = count / len(words)
    freqs.append(freq)
print(f"Chapter {i}: {freqs}")
# Plot the frequencies
plt.plot(freqs)
plt.xlabel("Chapter")
plt.ylabel("Frequency")
plt.title(f"Frequency of '{args.word}' in Moby Dick")
plt.show()
plt.savefig('plot.png')

