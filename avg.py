import csv
import json

# Open and read the book
with open("mobydick.txt", "r") as f:
    book = f.read()

# Split the book into chapters
chapters = book.split("CHAPTER ")

# Initialize a dictionary to store word counts for each chapter
chapter_word_counts = {}

# Open a CSV file for writing
with open("chapter_stats.csv", "w", newline="") as csvfile:
    csv_writer = csv.writer(csvfile)

    # Write CSV header
    csv_writer.writerow(["chapter-name", "chapter-length", "unique-words"])

    # Loop through each chapter and count occurrences of the word
    for i, chapter in enumerate(chapters):
        words = chapter.split()

        # Find unique words in the chapter and count occurrences of each
        unique_words = set(words)
        word_counts = {}
        for word in unique_words:
            word_counts[word] = words.count(word)

        # Store chapter name, length, and unique words in CSV
        csv_writer.writerow([f"Chapter {i+1}", len(words), len(unique_words)])

        # Store word counts for the chapter in dictionary
        chapter_word_counts[f"Chapter {i+1}"] = word_counts

# Output word counts as JSON
with open('word_counts.json', 'w') as json_file:
    json.dump(chapter_word_counts, json_file)

