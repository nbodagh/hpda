import os
import time

BLOCK_SIZE = 1024 * 1024  # 1 MiB

def test_read_write(path):
    with open(path, 'wb') as f:
        for i in range(10):
            data = os.urandom(BLOCK_SIZE)
            f.write(data)
        f.flush()

    with open(path, 'rb') as f:
        start = time.time()
        for i in range(10):
            f.read(BLOCK_SIZE)
        end = time.time()

    return end - start

path = 'testfile'
print(f'Write Time: {test_read_write(path):.6f}s')

with open(path, 'wb') as f:
    f.truncate(0)

with open(path, 'wb') as f:
    for i in range(10):
        data = os.urandom(BLOCK_SIZE)
        start = time.time()
        f.write(data)
        end = time.time()
    f.flush()

print(f'Read Time: {test_read_write(path):.6f}s')

os.remove(path)

