CREATE TABLE fact_table (
IP_address_id INT,
access_date_id INT,
time_spent INT
);

CREATE TABLE ip_address_dimension (
IP_address_id INT PRIMARY KEY,
country_id INT,
city_id INT,
browser_id INT,
user_agent_id INT
);

CREATE TABLE access_date_dimension (
access_date_id INT PRIMARY KEY,
access_date DATE
);

CREATE TABLE country_dimension (
country_id INT PRIMARY KEY,
country_name VARCHAR(255)
);

CREATE TABLE city_dimension (
city_id INT PRIMARY KEY,
city_name VARCHAR(255)
);

CREATE TABLE browser_dimension (
browser_id INT PRIMARY KEY,
browser_name VARCHAR(255)
);

CREATE TABLE user_agent_dimension (
user_agent_id INT PRIMARY KEY,
user_agent_name VARCHAR(255)
);
ALTER TABLE fact_table
ADD FOREIGN KEY (IP_address_id) REFERENCES ip_address_dimension(IP_address_id);

ALTER TABLE fact_table
ADD FOREIGN KEY (access_date_id) REFERENCES access_date_dimension(access_date_id);

ALTER TABLE ip_address_dimension
ADD FOREIGN KEY (country_id) REFERENCES country_dimension(country_id);

ALTER TABLE ip_address_dimension
ADD FOREIGN KEY (city_id) REFERENCES city_dimension(city_id);

ALTER TABLE ip_address_dimension
ADD FOREIGN KEY (browser_id) REFERENCES browser_dimension(browser_id);

ALTER TABLE ip_address_dimension
ADD FOREIGN KEY (user_agent_id) REFERENCES user_agent_dimension(user_agent_id);
