#!/usr/bin/env python3

import sys
import string
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer

def tokenize(text):
    # Tokenize the text into words
    words = text.split()
    # Remove punctuation and convert to lowercase
    table = str.maketrans('', '', string.punctuation)
    words = [word.translate(table).lower() for word in words]
    return words

def stem(word):
    # Stem a word using Porter Stemmer
    stemmer = PorterStemmer()
    return stemmer.stem(word)

def lemmatize(word):
    # Lemmatize a word using WordNet Lemmatizer
    lemmatizer = WordNetLemmatizer()
    return lemmatizer.lemmatize(word)

def main():
    # Process input from stdin
    for line in sys.stdin:
        line = line.strip()
        if line:
            # Split the line into filename and text
            filename, text = line.split(',', 1)
            # Tokenize the text and emit stemmed and lemmatized words with count of 1
            words = tokenize(text)
            for word in words:
                stemmed_word = stem(word)
                lemma_word = lemmatize(word)
                print(f'S,{filename},{stemmed_word},1')
                print(f'L,{filename},{lemma_word},1')

if __name__ == '__main__':
    main()

