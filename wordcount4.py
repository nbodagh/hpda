import nltk
import sys
import os
import string
from collections import Counter
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer

def tokenize(text):
    # Tokenize the text into words
    words = text.split()
    # Remove punctuation and convert to lowercase
    table = str.maketrans('', '', string.punctuation)
    words = [word.translate(table).lower() for word in words]
    return words

def stem(word):
    # Stem a word using Porter Stemmer
    stemmer = PorterStemmer()
    return stemmer.stem(word)

def lemmatize(word):
    # Lemmatize a word using WordNet Lemmatizer
    lemmatizer = WordNetLemmatizer()
    return lemmatizer.lemmatize(word)

def process_file(filename):
    # Process a single file
    word_counts = Counter()
    with open(filename, 'r') as f:
        text = f.read()
        # Tokenize the text and count the occurrences of each word
        words = tokenize(text)
        word_counts.update(words)
    # Compute total word count
    total_words = sum(word_counts.values())
    # Compute stemmed and lemmatized word counts
    stemmed_counts = Counter()
    lemma_counts = Counter()
    for word, count in word_counts.items():
        stemmed_word = stem(word)
        lemma_word = lemmatize(word)
        stemmed_counts[stemmed_word] += count
        lemma_counts[lemma_word] += count
    # Format output
    stemmed_list = [f"{stemmed_word}:{count}" for stemmed_word, count in stemmed_counts.items()]
    lemma_list = [f"{lemma_word}:{count}" for lemma_word, count in lemma_counts.items()]
    output = f'"{filename}",{total_words},"[{", ".join(stemmed_list)}]","[{", ".join(lemma_list)}]"'
    return output, stemmed_counts, lemma_counts

def process_files(filenames):
    # Process a list of files
    stemmed_counts = Counter()
    lemma_counts = Counter()
    outputs = []
    for filename in filenames:
        if os.path.isfile(filename):
            output, file_stemmed_counts, file_lemma_counts = process_file(filename)
            outputs.append(output)
            stemmed_counts.update(file_stemmed_counts)
            lemma_counts.update(file_lemma_counts)
    # Compute total stemmed and lemmatized counts
    total_stemmed_counts = sum(stemmed_counts.values())
    total_lemma_counts = sum(lemma_counts.values())
    # Write the total counts to a CSV file
    with open('word_counts.csv', 'w') as f:
        f.write('Type,Word,Count\n')
        f.write(f'S,Total,{total_stemmed_counts}\n')
        f.write(f'L,Total,{total_lemma_counts}\n')
        for word, count in stemmed_counts.items():
            f.write(f'S,{word},{count}\n')
        for word, count in lemma_counts.items():
            f.write(f'L,{word},{count}\n')
    # Return outputs
    return outputs

if __name__ == '__main__':
    # Process the command line arguments
    filenames = sys.argv[1:]
    outputs = process_files(filenames)
    # Print the outputs
    for output in outputs:
        print(output)

